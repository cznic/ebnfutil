ebnfutil
========

Package ebnfutils provides some utilities for messing with EBNF grammars.

Installation:

    $ go get modernc.org/ebnfutil

Documentation: [http://godoc.org/modernc.org/ebnfutil](http://godoc.org/modernc.org/ebnfutil)
