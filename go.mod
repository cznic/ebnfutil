module modernc.org/ebnfutil

go 1.18

require (
	modernc.org/ebnf v1.1.0
	modernc.org/strutil v1.2.0
)
